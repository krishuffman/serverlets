package serverlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw=res.getWriter();
        response.setContentType("text/html");

        String user=req.getParameter("userName");
        String pass=req.getParameter("Password");

        if (user.equals("admin") && pass.equals("Passw0rd"))
            pw.println("Login Sucessful, please proceed.");
        else
            pw.println("Login failed, access denied.");
        pw.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("invalid attempt");
    }
}
